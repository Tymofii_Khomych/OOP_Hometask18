﻿namespace task4
{
    using MyNamespace;

    internal class Program
    {
        static void Main(string[] args)
        {
            MyClass myclass = new MyClass();
            myclass.Print();
        }
    }
}

namespace MyNamespace
{
    internal class MyClass
    {
        public void Print()
        {
            Console.WriteLine("Method from MyCLass");
        }
    }
}