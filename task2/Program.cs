﻿using System;
using task2_out;

namespace AccessModifiersExample
{

    internal class DerivedClass : BaseClass
    {
        public void Print()
        {
            Console.WriteLine("Public method from DerivedClass");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            BaseClass baseObj = new BaseClass();
            baseObj.PublicMethod();

            DerivedClass derivedObj = new DerivedClass();
            derivedObj.Print();
        }
    }
}
