﻿namespace OOP_Hometask18
{
    using Dictionary;
    internal class Program
    {
        static void Main(string[] args)
        {
            MyDictionary<string, int> myDictionary = new MyDictionary<string, int>();

            myDictionary.Add("One", 1);
            myDictionary.Add("Two", 2);
            myDictionary.Add("Three", 3);

            Console.WriteLine("Total count: " + myDictionary.Count);

            Console.WriteLine("Elements:");
            foreach (KeyValuePair<string, int> item in myDictionary)
            {
                Console.WriteLine(item.Key + ": " + item.Value);
            }

            Console.WriteLine("Value for key 'Two': " + myDictionary["Two"]);

        }
    }
}

namespace Dictionary
{
    using System.Collections;
    using System.Collections.Generic;

    internal class MyDictionary<TKey, TValue> : IEnumerable<KeyValuePair<TKey, TValue>>
    {
        private List<KeyValuePair<TKey, TValue>> items = new List<KeyValuePair<TKey, TValue>>();

        public void Add(TKey key, TValue value)
        {
            items.Add(new KeyValuePair<TKey, TValue>(key, value));
        }

        public TValue this[TKey key]
        {
            get
            {
                foreach (KeyValuePair<TKey, TValue> item in items)
                {
                    if (EqualityComparer<TKey>.Default.Equals(item.Key, key))
                    {
                        return item.Value;
                    }
                }
                throw new KeyNotFoundException();
            }
        }

        public int Count
        {
            get { return items.Count; }
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

}